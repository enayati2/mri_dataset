import os
import sys
import subprocess

def run_visualization(zip_file_path):
    # Run the visualization script
    visualization_command = f"python MRI_dataset.py {zip_file_path}"
    subprocess.run(visualization_command, shell=True, check=True)

def run_fine_tuning(zip_file_path, model_save_path):
    # Run the fine-tuning script
    fine_tune_command = f"python fine_tune.py {zip_file_path} {model_save_path}"
    subprocess.run(fine_tune_command, shell=True, check=True)

def run_prediction(zip_file_path, model_path):
    # Run the prediction script
    predict_command = f"python prediction.py {zip_file_path} {model_path}"
    subprocess.run(predict_command, shell=True, check=True)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python run_pipeline.py <zip_file_path> <model_save_path>")
        sys.exit(1)
    
    zip_file_path = sys.argv[1]
    model_save_path = sys.argv[2]

    # Run visualization
    print("Starting visualization...")
    run_visualization(zip_file_path)
    print("Visualization completed.")

    # Run fine-tuning
    print("Starting fine-tuning...")
    run_fine_tuning(zip_file_path, model_save_path)
    print("Fine-tuning completed.")

    # Run prediction
    print("Starting prediction...")
    run_prediction(zip_file_path, model_save_path)
    print("Prediction completed.")
