import sys
import os
import torch
import random
import numpy as np
from torchvision import models
import torch.optim as optim
from torch.optim import lr_scheduler
from torch import nn
from MRI_dataset import create_data_loaders, extract_zip

def set_random_seeds(seed=42):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

def fine_tune_model(train_loader, num_classes, model_save_path, num_epochs=1, learning_rate=0.001): ###num_epochs=25, changed to 1 to test

    # Set random seeds for reproducibility
    set_random_seeds()

    # Load a pre-trained model (e.g., ResNet18)
    model = models.resnet18(pretrained=True)

    # Modify the final layer to match the number of classes in our dataset
    model.fc = nn.Linear(model.fc.in_features, num_classes)

    # Move the model to GPU if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    # Loss function and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)

    # Training loop
    for epoch in range(num_epochs):
        print(f'Epoch {epoch+1}/{num_epochs}')
        print('-' * 10)

        model.train()  # Set model to training mode
        running_loss = 0.0
        running_corrects = 0

        # Iterate over data
        for inputs, labels in train_loader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            # Zero the parameter gradients
            optimizer.zero_grad()

            # Forward
            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)
            loss = criterion(outputs, labels)

            # Backward + optimize
            loss.backward()
            optimizer.step()

            # Statistics
            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels.data)

        scheduler.step()

        epoch_loss = running_loss / len(train_loader.dataset)
        epoch_acc = running_corrects.double() / len(train_loader.dataset)

        print(f'Train Loss: {epoch_loss:.4f} Acc: {epoch_acc:.4f}')

        # Save the fine-tuned model at the end of each epoch (overwrite the same file)
        torch.save(model.state_dict(), model_save_path)
        print(f'Model saved to {model_save_path}')

    print('Training complete')

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python train_model.py <zip_file_path> <model_save_path>")
        sys.exit(1)
    
    zip_file_path = sys.argv[1]
    model_save_path = sys.argv[2]

    # Define extraction directory
    extract_to = os.path.join(os.path.dirname(zip_file_path), 'extracted_data')
    
    # Extract the zip file using the function from MRI_dataset
    extract_zip(zip_file_path, extract_to)
    
    # Paths to the training directory inside the extracted folder
    train_dir = os.path.join(extract_to, 'Training')
    test_dir = os.path.join(extract_to, 'Testing')

    # Create data loaders
    train_loader, _ = create_data_loaders(train_dir, test_dir)

    # Get number of classes
    num_classes = len(train_loader.dataset.classes)

    # Fine-tune the model
    fine_tune_model(train_loader, num_classes, model_save_path)
