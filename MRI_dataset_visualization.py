import os
import sys
import zipfile
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
from torch.utils.data import DataLoader

def extract_zip(zip_path, extract_to):
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        zip_ref.extractall(extract_to)

def main(zip_file_path):
    # Define extraction directory
    extract_to = os.path.join(os.path.dirname(zip_file_path), 'extracted_data')
    
    # Extract the zip file
    extract_zip(zip_file_path, extract_to)
    
    # Paths to the training and testing directories inside the extracted folder
    train_dir = os.path.join(extract_to, 'Training')
    test_dir = os.path.join(extract_to, 'Testing')

    # transformations for the training and testing sets
    train_transform = transforms.Compose([
        transforms.Resize((224, 224)),  # Resize the images to 224x224
        transforms.RandomHorizontalFlip(),  # Randomly flip the images horizontally
        transforms.ToTensor(),  # Convert the images to tensors
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])  # Normalize with ImageNet standards
    ])

    test_transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])

    # Create datasets
    train_dataset = datasets.ImageFolder(train_dir, transform=train_transform)
    test_dataset = datasets.ImageFolder(test_dir, transform=test_transform)

    # Create data loaders
    train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True, num_workers=4)
    test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False, num_workers=4)

    # Classes
    classes = train_dataset.classes

    # Print some information about the dataset
    print(f"Classes: {classes}")
    print(f"Number of training samples: {len(train_dataset)}")
    print(f"Number of testing samples: {len(test_dataset)}")

    # Example usage
    for images, labels in train_loader:
        print(f"Batch of images shape: {images.shape}")
        print(f"Batch of labels shape: {labels.shape}")
        break

    # Visualize a sample
    # first need to denormalize the image
    def denormalize(image, mean, std):
        image = image.clone()  # Avoid modifying the original image
        for t, m, s in zip(image, mean, std):
            t.mul_(s).add_(m)
        return image

    # Get one batch of training images
    images, labels = next(iter(train_loader))

    # Get one image from the batch
    image = images[0]
    label = labels[0]

    # Denormalize the image
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    image = denormalize(image, mean, std)

    # Convert the tensor to a numpy array
    image = image.numpy().transpose((1, 2, 0))

    # Plot the image
    plt.imshow(image)
    plt.title(f'Class: {classes[label]}')
    plt.axis('off')
    plt.show()

    # Save the image to a file
    output_file = 'sample_image.png'
    plt.savefig(output_file)
    print(f"Sample image saved to {output_file}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <zip_file_path>")
        sys.exit(1)
    
    zip_file_path = sys.argv[1]

    main(zip_file_path)
