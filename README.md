

# Finetune on Brain Tumor MRI Dataset 

This project provides Python scripts to finetune Resnet18 model on  MRI images of brain tumors from a dataset. The dataset is expected to be in a zip file format containing two subfolders for training and testing images.

## Requirements






## Usage

1- Clone the repository:

 * git clone https://gitlab.gwdg.de/enayati2/mri_dataset

2- Locate to directory of the repository:

 * cd mri_dataset

3- Ensure the dataset zip file is in the repository directory. If not, move it there:
 * mv /path/to/your/Brain_Tumor_MRI.zip

4- Make the environment which include all Requirements and activate it:
 * conda env create -f environment.yml
 * conda activate mri_env


3- Run the script-wise:
 * python MRI_dataset.py path/to/your_mri_data.zip
 * python train_model.py path/to/your_mri_data.zip path/to/save/fine_tuned_model.pth
 * python prediction.py path/to/your_mri_data.zip path/to/saved/fine_tuned_model.pth

4- Run the pipeline:
 * python run_pipeline.py path/to/data.zip path/to/save_model.pth


## Authors and acknowledgment

## License

## Project status
Inital phase. Dataset will be changed to real data and the model for fine-tuning will be changed to best existing and available one.

