import sys
import os
import torch
import random
import numpy as np
from torchvision import models
from torch import nn
from MRI_dataset import create_data_loaders

def set_random_seeds(seed=42):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

def load_model(model_path, num_classes):
    # Load a pre-trained model (e.g., ResNet18)
    model = models.resnet18(pretrained=False)

    # Modify the final layer to match the number of classes in our dataset
    model.fc = nn.Linear(model.fc.in_features, num_classes)

    # Load the trained weights
    model.load_state_dict(torch.load(model_path))
    
    return model

def predict(model, test_loader):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)
    model.eval()  # Set model to evaluate mode

    all_preds = []
    all_labels = []
    all_class_names = []

    with torch.no_grad():
        for inputs, labels in test_loader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            all_preds.extend(preds.cpu().numpy())
            all_labels.extend(labels.cpu().numpy())
            all_class_names.extend(test_loader.dataset.classes)  # Collect all class names

    return all_preds, all_labels, all_class_names

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python predict_model.py <zip_file_path> <model_path>")
        sys.exit(1)
    
    zip_file_path = sys.argv[1]
    model_path = sys.argv[2]

     # Set random seeds for reproducibility
    set_random_seeds()

    # Define extraction directory
    extract_to = os.path.join(os.path.dirname(zip_file_path), 'extracted_data')
    
    # Extract the zip file using the function from MRI_dataset
    from MRI_dataset import extract_zip
    extract_zip(zip_file_path, extract_to)
    
    # Paths to the testing directory inside the extracted folder
    train_dir = os.path.join(extract_to, 'Training')
    test_dir = os.path.join(extract_to, 'Testing')

    # Create data loaders
    _, test_loader = create_data_loaders(train_dir, test_dir)

    # Get number of classes
    num_classes = len(test_loader.dataset.classes)

    # Load the fine-tuned model
    model = load_model(model_path, num_classes)

    # Predict on the test set
    predictions, labels, class_names = predict(model, test_loader)

    # Map numeric predictions and labels to class names
    pred_class_names = [class_names[pred] for pred in predictions]
    label_class_names = [class_names[label] for label in labels]

    # Print predictions and actual labels with class names
    #print("Predictions:", pred_class_names)
    #print("Actual Labels:", label_class_names)

    # Calculate accuracy
    correct_preds = sum(p == l for p, l in zip(pred_class_names, label_class_names))
    accuracy = correct_preds / len(labels)
    print(f'Accuracy: {accuracy:.4f}')